import time
from threading import *
from Recording.Eye.EyeTracker import *
from Recording.DataFile import *
from Recording.Mouse.MousePos import *
from Recording.Eye.MyGazeErrors import handleerror


# API Inladen
mygazeapi = load_api()

#  Variabelen
dps = 60  # data per second
data_seconds = 0  # hoeveel seconden zijn we data aan het tracken
recording = False  # Zijn we op het moment aan het recorden?
stopdatastreamrequest = False  # wanneer deze variabele naar True wordt gezet zal de datastream worden gestopt.
time_stamp = 0 # de tijdstempel die aangeeft hoe ver we zijn in de recording

# alle files waarin we data gaan schrijven

f_eye = -1
f_mouse_pos = -1
f_mouse_click = -1
f_keyboard = -1


def recording_loop():
    global dps, data_seconds, stopdatastreamrequest, recording, time_stamp

    start_time = time.time()
    i = 1  # increments
    r = 1/dps  # rate
    time_stamp = 0
    data_seconds = 0

    while recording is True:
        if time.time()-start_time >= (i*r):

            # haal alle data op

            lefteye = CEye(0, 0, 0, 0, 0, 0)
            righteye = CEye(0, 0, 0, 0, 0, 0)
            eye_sample = CSample(0, lefteye, righteye)
            mygazeapi.iV_GetSample(byref(eye_sample))

            mouse_pos_x = get_mouse_position("X")
            mouse_pos_y = get_mouse_position("Y")

            # schrijf de data

            f_eye.write(str(time_stamp) + ": X " + str(eye_sample.leftEye.gazeX) + " Y " +
                    str(eye_sample.leftEye.gazeY) + "\n")
            f_mouse_pos.write(str(time_stamp) + ": X " + str(mouse_pos_x) + " Y " +
                    str(mouse_pos_y) + "\n")

            # ga naar de volgende time stamp
            i += 1
            time_stamp += 1
        if i is dps:

            # haal alle data op

            lefteye = CEye(0, 0, 0, 0, 0, 0)
            righteye = CEye(0, 0, 0, 0, 0, 0)
            eye_sample = CSample(0, lefteye, righteye)
            mygazeapi.iV_GetSample(byref(eye_sample))

            mouse_pos_x = get_mouse_position("X")
            mouse_pos_y = get_mouse_position("Y")

            # schrijf de data

            f_eye.write(str(time_stamp) + ": X " + str(eye_sample.leftEye.gazeX) + " Y " +
                    str(eye_sample.leftEye.gazeY) + "\n")
            f_mouse_pos.write(str(time_stamp) + ": X " + str(mouse_pos_x) + " Y " +
                    str(mouse_pos_y) + "\n")

            # er is een seconde voorbij gegaan, reset de timers

            i = 1
            start_time = time.time()
            data_seconds += 1
            time_stamp += 1

            print("De Recorder is "+str(data_seconds)+" seconden aan het opnemen.")
            if stopdatastreamrequest is True:
                recording = False
                f_eye.close
                print("Data stream gestopt. Er is %s data geschreven in %s seconden. (%s data per seconde)"
                      % (time_stamp, data_seconds, time_stamp/data_seconds))
                stopdatastreamrequest = False
                return


def start_recording():
    global f_eye, f_mouse_pos, recording
    if len(handleerror(mygazeapi.iV_IsConnected())) > 0:
        print("MyGaze EyeTracker is niet verbonden, kan recording niet starten.")
        return

    f_eye = open(get_save_directory()+"/"+get_save_name()+"_eye.txt", "w")
    f_mouse_pos = open(get_save_directory()+"/"+get_save_name()+"_mouse_pos.txt", "w")
    recording = True
    t = Thread(target=recording_loop)
    t.start()


def stop_recording():
    global stopdatastreamrequest
    stopdatastreamrequest = True