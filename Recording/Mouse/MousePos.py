from ctypes import windll, Structure, c_ulong, byref


user32 = windll.user32

screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)


class POINT(Structure):
    _fields_ = [("x", c_ulong), ("y", c_ulong)]


def get_mouse_position(coord):
    pt = POINT()
    windll.user32.GetCursorPos(byref(pt))

    x = 100.0 * (pt.x + 1) / user32.GetSystemMetrics(0)
    y = 100.0 * (pt.y + 1) / user32.GetSystemMetrics(1)

    if coord is "X":
        return x
    if coord is "Y":
        return y
    return -1
