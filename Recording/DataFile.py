from tkinter import filedialog
from os import getcwd
"""
De DataFile klasse bevat alle methodes die we nodig hebben om het bestand te schrijven/opslaan
"""

# variabelen

save_directory = getcwd()
save_name = "data"


# methodes


def set_save_directory():
    global save_directory
    save_directory = filedialog.askdirectory()


def get_save_directory():
    global save_directory
    return save_directory


def set_save_name(n):
    global save_name
    save_name = n


def get_save_name():
    global save_name
    return save_name
