from ctypes import *
from Recording.Eye.MyGazeErrors import handleerror


# defineer MyGaze klassen
class CEye(Structure):
    _fields_ = [("gazeX", c_double),
                ("gazeY", c_double),
                ("diam", c_double),
                ("eyePositionX", c_double),
                ("eyePositionY", c_double),
                ("eyePositionZ", c_double)]


class CSample(Structure):
    _fields_ = [("timestamp", c_longlong),
                ("leftEye", CEye),
                ("rightEye", CEye)]


def load_api():
    global sampledata
    lefteye = CEye(0, 0, 0, 0, 0, 0)
    righteye = CEye(0, 0, 0, 0, 0, 0)
    sampledata = CSample(0, lefteye, righteye)
    mygazeapi = windll.LoadLibrary("myGazeAPI.dll")
    if mygazeapi is None:
        print("Failed to load myGazeAPI")
        return None
    else:
        print("Successfully loaded myGazeAPI")
        return mygazeapi


# verbinden met de eyetracker
def connect_eyetracker(mygazeapi):
    return handleerror(mygazeapi.iV_Connect())


def calibrate_eyetracker(mygazeapi):
    mygazeapi.iV_SetupCalibration(0)
    e = mygazeapi.iV_Calibrate()
    if len(handleerror(e)) == 0:
        print("Calibration completed.")
    else:
        print(handleerror(e))
