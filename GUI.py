from tkinter import *
from Recording.Recorder import *
from Recording.Eye.EyeTracker import *

# De MyGaze API inladen en met het balkje verbinden


# status variable laat zien in de GUI of we zijn verbonden met het balkje
status = "niet verbonden."
# als we kunnen verbinden met de eyetracker...
if len(connect_eyetracker(mygazeapi)) == 0:
    status = "verbonden."


# De GUI (root is de window die wordt gedisplayed
root = Tk()
root.title("NeurUX")

# GUI Functies

# Deze functie haalt de naam die is ingevoerd in de GUI op


def get_name_entry():
    if len(opslaan_entry.get()) > 0:
        return opslaan_entry.get()
    return "neurux_data"


# Wat er gebeurt als je op de start knop klikt
def handlestartbutton():
    set_save_name(get_name_entry())
    start_recording()

# GUI widgets/onderdelen

opslaan_entry = Entry(root)
start_button = Button(root, text="Start", command=lambda: handlestartbutton())
stop_button = Button(root, text="Stop", command=lambda: stop_recording())
calibreer_button = Button(root, text="Calibreer", command=lambda: calibrate_eyetracker(mygazeapi))
instellingen = Label(root, text="Instellingen")
dataps = Label(root, text="Data p/s")
locatie = Label(root, text="Locatie")
opslaan = Label(root, text="Opslaan als")
bladeren_button = Button(root, text="Bladeren", command=set_save_directory)
status_label = Label(root, text="MyGaze EyeTracker is "+status)

start_button.grid(row=0)
stop_button.grid(row=0, column=1)
calibreer_button.grid(row=0, column=2)
instellingen.grid(row=1, column=1)
dataps.grid(row=2)
locatie.grid(row=3)
opslaan.grid(row=2)
opslaan_entry.grid(row=2, column=1)
bladeren_button.grid(row=3, column=1)
status_label.grid(row=5, column=1)

root.mainloop()
